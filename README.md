# Servicios y aplicaciones alternativos y libres

This is the project for the slides of the talk at FLISoL 2021 Tenerife called "Servicios y aplicaciones alternativos y libres" by Brais Arias Rio

You can see the slides here: https://braisarias.gitlab.io/servicios-y-aplicaciones-alternativos-y-libres

# Descripción

Hoy en día, todo el mundo hace uso de servicios y aplicaciones en internet, pero ¿dónde están mis datos? ¿Están seguros allí? ¿Quién tiene acceso a mis datos?

Estas son algunas de las preguntas que se hace mucha gente preocupada por la privacidad o por la soberanía tecnológica.

¡Basta ya de solo pensar! ¡Da el paso!
¡Deja de usar esos servicios y aplicaciones que sabes que se aprovechan de ti!

En esta charla se explicarán las claves para dejar de usar los servicios y aplicaciones de los oligopolios de internet y ser libres.



# License
The contento of this slides are under the license CC BY-SA, you can see all the legal code here: https://creativecommons.org/licenses/by/4.0/legalcode


# Authors

Brais Arias Rio
